var searchData=
[
  ['max_5fiters',['max_iters',['../structNNParams.html#af1ca6425073149941b02ca9c8bc1b4e7',1,'NNParams::max_iters()'],['../structPFParams.html#a247f6354e907eac4019b5f194dc00e53',1,'PFParams::max_iters()']]],
  ['max_5fsimilarity',['max_similarity',['../classPF.html#a8d03c6cfb90bf9f711d5a80aac777835',1,'PF']]],
  ['max_5fwt_5fid',['max_wt_id',['../classPF.html#a6ccad6776f706d957ab7b2f878d18d33',1,'PF']]],
  ['mean_5ftype',['mean_type',['../structPFParams.html#adef741dc9861c4a22ee8543d0b383221',1,'PFParams']]],
  ['min_5fparticles_5fratio',['min_particles_ratio',['../structPFParams.html#a16a7f4688337858b1ef814f08956afba',1,'PFParams']]]
];
