var searchData=
[
  ['n_5fbins',['n_bins',['../structCCREParams.html#afeb767eb97c56f06a0b1953a741fffa2',1,'CCREParams::n_bins()'],['../structLKLDParams.html#ae8b0eb3d9be40d306935dfeeef2c6077',1,'LKLDParams::n_bins()'],['../structLRSCVParams.html#a0b19238d280c8c178d54ddc8f991286f',1,'LRSCVParams::n_bins()'],['../structLSCVParams.html#aa3ff2ada51e5ee8693bb3fdf59700974',1,'LSCVParams::n_bins()'],['../structMIParams.html#a2e0e6ee1bd5cab5498d929798333798d',1,'MIParams::n_bins()'],['../structRSCVParams.html#a8aaca4a91a8b7227e611d1706a8a7448',1,'RSCVParams::n_bins()'],['../structSCVParams.html#a08402ab19dd0c54cb7a7a069dd850fb1',1,'SCVParams::n_bins()']]],
  ['n_5fbounding_5fpts',['n_bounding_pts',['../classSpline.html#a0b91fab8f2dae929fc3667ef72e5a34f',1,'Spline']]],
  ['n_5fchannels',['n_channels',['../classImageBase.html#ac16eaa560cdf51fc9232ae115c3567e2',1,'ImageBase']]],
  ['n_5ffeat',['n_feat',['../structPCAParams.html#a407e786b6ed9d47b930c6a1f817514f8',1,'PCAParams']]],
  ['n_5fparticles',['n_particles',['../structPFParams.html#a477fbc13d753c5a2567897c21fca0815',1,'PFParams']]],
  ['n_5fpix',['n_pix',['../classImageBase.html#a01bbe6e54a59a6a54885222225233040',1,'ImageBase']]],
  ['n_5fregions_5fx',['n_regions_x',['../structPGBParams.html#a2a59877a038f654df446e81aa203ec43',1,'PGBParams']]],
  ['n_5fsamples',['n_samples',['../structmtf_1_1RegNetParams.html#a7f6b73f2e2d05deea9beed88ca59d7b1',1,'mtf::RegNetParams']]],
  ['n_5fsub_5fregions_5fx',['n_sub_regions_x',['../structLKLDParams.html#a76a8f63d923db7230c450ea6f0eac50c',1,'LKLDParams']]],
  ['name',['name',['../classAppearanceModel.html#a593c80caffb59c01fff7dcb820d57285',1,'AppearanceModel']]],
  ['ncc',['NCC',['../classNCC.html',1,'']]],
  ['nccparams',['NCCParams',['../structNCCParams.html',1,'NCCParams'],['../structNCCParams.html#a792823877cfb2c7c3e2cc8b29e19a0a7',1,'NCCParams::NCCParams(const AMParams *am_params, bool _fast_hess, double _likelihood_alpha)'],['../structNCCParams.html#a42ac2c56df37193dbdd567eab5ad1f06',1,'NCCParams::NCCParams(const NCCParams *params=nullptr)']]],
  ['nn',['NN',['../classNN.html',1,'']]],
  ['nnparams',['NNParams',['../structNNParams.html',1,'']]],
  ['node',['Node',['../structgnn_1_1Node.html',1,'gnn']]],
  ['norm_5fpix_5fmax',['norm_pix_max',['../structNSSDParams.html#a19c25cc0bffff4524dfb5db7ebba5e63',1,'NSSDParams']]],
  ['norm_5fpix_5fmin',['norm_pix_min',['../structNSSDParams.html#a4abf2a25a739f0b45c20e4728cf8cbad',1,'NSSDParams']]],
  ['nssd',['NSSD',['../classNSSD.html',1,'']]],
  ['nssdparams',['NSSDParams',['../structNSSDParams.html',1,'NSSDParams'],['../structNSSDParams.html#acd5af96c8a4656b49124db3c65307c59',1,'NSSDParams::NSSDParams(const AMParams *am_params, double _norm_pix_max, double _norm_pix_min, bool _debug_mode)'],['../structNSSDParams.html#a5ac90a50027d4d8ba7fb1c1757e46444',1,'NSSDParams::NSSDParams(const NSSDParams *params=nullptr)']]]
];
