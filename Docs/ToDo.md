* Windows support - adapt GNU make/cmake build system for Visual Studio
* Matlab front end
* HTML/Javascript front end for web based demo
* Augmented reality application
* Add more feature types (SURF, HOG, BRISK, FAST, ...) to GridTrackerFeat
* Multi channel support for LRSCV
* Channel wise implementations of multi channel support for all AMs especially MI and CCRE
* Complete KL Divergence AM (KLD) and its localized version (LKLD)
* Improve Doxygen documentation
* Selective pixel integration (SPI) support for all AMs and SSMs
* RBF illumination model
* Complete Lie Affine SSM
* Complete Spline and TPS SSM
* Add piecewise projective and affine SSMs
