#ifndef MTF_PIPELINE_H
#define MTF_PIPELINE_H

// tools for capturing images from disk or cameras
#include "mtf/Tools/inputCV.h"
#ifndef DISABLE_THIRD_PARTY_TRACKERS
#ifndef DISABLE_XVISION
#include "mtf/Tools/inputXV.h"
#endif
#ifndef DISABLE_VISP
#include "mtf/Tools/inputVP.h"
#endif
#endif
// tools for preprocessing the image
#include "mtf/Tools/PreProc.h"

#include "mtf/Config/parameters.h"

#include <memory>

#ifndef HETEROGENEOUS_INPUT
#define HETEROGENEOUS_INPUT -1
#endif

#define OPENCV_PIPELINE 'c'
#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#ifndef DISABLE_VISP
#define VISP_PIPELINE 'v'
#endif

using namespace mtf::params;

typedef std::unique_ptr<InputBase> InputBase_;
typedef PreProc::Ptr PreProc_;

inline InputBase* getInputObj(char _pipeline_type){
	switch(_pipeline_type){
	case OPENCV_PIPELINE:
		return new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
#ifndef DISABLE_XVISION
	case XVISION_PIPELINE:
		return new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
#endif
#ifndef DISABLE_VISP
	case VISP_PIPELINE:
		return new InputVP(
			img_source, source_name, source_fmt, source_path, buffer_count, visp_usb_n_buffers,
			static_cast<VpResUSB>(visp_usb_res), static_cast<VpFpsUSB>(visp_usb_fps),
			static_cast<VpResFW>(visp_fw_res), static_cast<VpFpsFW>(visp_fw_fps)
			);
#endif
	default:
		throw std::invalid_argument(
			cv::format("Invalid pipeline type specified: %c\n",
			_pipeline_type));
	}
}

inline PreProc* createPreProcObj(int output_type, const std::string &_pre_proc_type){
	if(_pre_proc_type == "-1" || _pre_proc_type == "none"){
		return new NoProcessing(output_type, img_resize_factor, pre_proc_hist_eq);
	} else if(_pre_proc_type == "0" || _pre_proc_type == "gauss"){
		return new GaussianSmoothing(output_type, img_resize_factor, pre_proc_hist_eq,
			gauss_kernel_size, gauss_sigma_x, gauss_sigma_y);
	} else if(_pre_proc_type == "1" || _pre_proc_type == "med"){
		return new MedianFiltering(output_type, img_resize_factor, pre_proc_hist_eq, med_kernel_size);
	} else if(_pre_proc_type == "2" || _pre_proc_type == "box"){
		return new NormalizedBoxFltering(output_type, img_resize_factor, pre_proc_hist_eq, box_kernel_size);
	} else if(_pre_proc_type == "3" || _pre_proc_type == "bil"){
		return new BilateralFiltering(output_type, img_resize_factor, pre_proc_hist_eq,
			bil_diameter, bil_sigma_col, bil_sigma_space);
	} else{
		throw std::invalid_argument(
			cv::format("Invalid image pre processing type specified: %s\n",
			_pre_proc_type.c_str()));
	}
}

inline PreProc_ getPreProcObj(const vector<PreProc_> &existing_objs, int output_type,
	const std::string &_pre_proc_type){
	if(output_type == HETEROGENEOUS_INPUT){
		PreProc_ new_obj = getPreProcObj(existing_objs, supported_output_types[0], _pre_proc_type);
		PreProc_ curr_obj = new_obj;
		for(int output_id = 1; output_id < supported_output_types.size(); ++output_id){
			curr_obj->next = getPreProcObj(existing_objs, supported_output_types[output_id], _pre_proc_type);
			curr_obj = curr_obj->next;
		}
		return new_obj;
	}
	for(int obj_id = 0; obj_id < existing_objs.size(); ++obj_id){
		for(PreProc_ curr_obj = existing_objs[obj_id]; curr_obj; curr_obj = curr_obj->next){
			if(curr_obj->outputType() == output_type){ return curr_obj; }
		}
	}
	return PreProc_(createPreProcObj(output_type, _pre_proc_type));
}
inline PreProc_ getPreProcObj(int output_type,
	const std::string &_pre_proc_type){
	if(output_type == HETEROGENEOUS_INPUT){
		PreProc_ new_obj = getPreProcObj(supported_output_types[0], _pre_proc_type);
		PreProc_ curr_obj = new_obj;
		for(int output_id = 1; output_id < supported_output_types.size(); ++output_id){
			curr_obj->next = getPreProcObj(supported_output_types[output_id], _pre_proc_type);
			curr_obj = curr_obj->next;
		}
		return new_obj;
	}
	return PreProc_(createPreProcObj(output_type, _pre_proc_type));

}

#endif