#include <fstream>      // std::ifstream
#include <iostream>     // std::cout
#include <time.h>
#include <assert.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "mtf/AM/PCA.h"
#include "mtf/Utilities/imgUtils.h"
// #define DEBUG

_MTF_BEGIN_NAMESPACE

//! value constructor
PCAParams::PCAParams(const AMParams *am_params,
string _actor,
string _seq_name,
int _n_eigenvec,
int _batch_size,
float _f_factor):
AMParams(am_params),
actor(_actor),
seq_name(_seq_name),
n_eigenvec(_n_eigenvec),
batch_size(_batch_size),
f_factor(_f_factor){}

//! default/copy constructor
PCAParams::PCAParams(const PCAParams *params) :
AMParams(params),
actor(PCA_ACTOR),
seq_name(PCA_SEQ_NAME),
n_eigenvec(PCA_N_EIGENVEC),
batch_size(PCA_BATCHSIZE),
f_factor(PCA_F_FACTOR)
{
	if(params){
		actor = params->actor;
		seq_name = params->seq_name;
		n_eigenvec = params->n_eigenvec;
		batch_size = params->batch_size;
		f_factor = params->f_factor;
	}
}

PCA::PCA(const ParamType *PCA_params) :
SSDBase(PCA_params), params(PCA_params){
	printf("\nInitializing PCA AM with...\n");
	name = "PCA";
	n_eigenvec = params.n_eigenvec;
	batch_size = params.batch_size;
	f_factor = params.f_factor;
}

double PCA::getLikelihood() const{
	// Convert to likelihood for particle filters.
	// Normalize over the number of pixels
	double result = exp(-sqrt(-f / static_cast<double>(n_pix)));
#ifdef DEBUG
	std::cout << "The likelihood at frame " << frame_count << " is " << result << std::endl;
#endif
	return result;
}

void PCA::initializeSimilarity() {
	SSDBase::initializeSimilarity();
	printf("Initialize PCA ");
	printf("at frame %d.\n", frame_count);
	addi_patches = getInitPixVals();
	U_available = false;
	cv::Scalar gt_color(0, 255, 0);
	VectorXd init_patch = addi_patches.col(0);
	cv::Mat template_cv = cv::Mat(getResY(), getResX(), CV_64FC1, init_patch.data());
	// Construct Mat object for the uint8 gray scale patch
	cv::Mat template_cv_uchar(getResY(), getResX(), CV_8UC1);
	// Convert the RGB patch to grayscale
	double min_val = init_patch.minCoeff();
	double max_val = init_patch.maxCoeff();
	double range = max_val - min_val;
	template_cv.convertTo(template_cv_uchar, template_cv_uchar.type(),255./range, -255.*min_val/range);
	// Make a bigger image
	cv::Mat template_cv_uchar_bigger(5*getResY(), 5*getResX(), CV_8UC1);
	cv::resize(template_cv_uchar, template_cv_uchar_bigger, template_cv_uchar_bigger.size() );
	cv::putText(template_cv_uchar_bigger, "Template", cv::Point2d(5,15),
					cv::FONT_HERSHEY_SIMPLEX, 0.5 , gt_color);
	imshow("Template Image", template_cv_uchar_bigger);
}

//! Similarity: negative number, the higher the score, the more similar it is
void PCA::updateSimilarity(bool prereq_only/*=true*/) {
  #ifdef DEBUG
	cout << "updateSimilarity at frame " <<  frame_count << endl;
  #endif

	// update similarity of each particle
	if (U_available) {
	  // if having U, update similarity of each frame using newest eigenbasis
		// error = |I_t - U U^T I_t| in an optimized way
		//    refer to https://eigen.tuxfamily.org/dox/group__TopicAliasing.html
		//             for how to use noalias() to make efficient computation
		VectorXd current_img = getCurrPixVals();
		current_img -= mean_prev_patches;
		I_diff = current_img;
		I_diff.noalias() -= U * (U.transpose() * current_img);
		if (first_particle == true) {
			display_images(current_img, I_diff);
		}
	}
	else {
		// if not having U, using SSD
		I_diff = getCurrPixVals() - getInitPixVals();
	}

	// if(prereq_only){ return; }
	f = -I_diff.squaredNorm()/2;
	// initialize max_patch_eachframe when the first particle in the current frame comes
	if (first_particle == true) {
		max_patch_eachframe = getCurrPixVals();
		first_particle = false;
		max_f = f;
	}
	// if not the first particle and if the similarity of this particle is lower (more similar),
	// update the max patch
	else if ( f > max_f) {
		max_patch_eachframe = getCurrPixVals();
		max_f = f;
	}
  #ifdef DEBUG
	std::cout << "The similarity is " << f << std::endl;
  #endif

}

void PCA::setFirstIter() {
	first_iter = true;
	frame_count++;
	first_particle = true;
  #ifdef DEBUG
	printf("It's the first particle at frame %d\n", frame_count);
  #endif
}

void PCA::clearFirstIter() {
	first_iter = false;
	// update the basis
	updateBasis();
}

void PCA::updateBasis() {
	// update addi_patches before batch_size number of frames
	if (frame_count < batch_size) {
		addi_patches.conservativeResize(NoChange, addi_patches.cols() + 1);
		addi_patches.col(addi_patches.cols()-1) = max_patch_eachframe;
	}
	else if (frame_count == batch_size){
		// if just had enough images to compute the initial SVD
		n_prev_patches = batch_size;
		// append current patch to addi_patches
		addi_patches.conservativeResize(NoChange, addi_patches.cols() + 1);
		addi_patches.col(addi_patches.cols()-1) = max_patch_eachframe;
		mean_prev_patches = addi_patches.rowwise().mean();
		// computer SVD
		JacobiSVD < MatrixXd > svd(addi_patches.colwise()-mean_prev_patches, ComputeThinU | ComputeThinV);
		U = svd.matrixU();
		sigma = svd.singularValues();
		U_available = true;
		display_basis();
	}
	else {
		if ( 1 == (frame_count % batch_size) ) {
			// only have one additional patch at this point
			addi_patches = max_patch_eachframe;
		}
		else {
			// first update addi_patches
			addi_patches.conservativeResize(NoChange, addi_patches.cols() + 1);
			addi_patches.col(addi_patches.cols()-1) = max_patch_eachframe;
			if( 0 == (frame_count % batch_size) ) {
				// if received enough frames, update new eigenbasis each batch_size of frames
				incrementalPCA();
				display_basis();
			}
		}

	}
}


// the core algorithm in ivt tracker: 
// sklm(Sequential Karhunen-Loeve Transform with Mean update)
// Notation follows the ivt paper Figure 1.
void PCA::sklm(MatrixXd &U, VectorXd &sigma, VectorXd &mu_A, MatrixXd &B, int &n, float ff, int max_n_eig){
	// step 1
	// mean of additional patches
	int m = B.cols();
	VectorXd mu_B = B.rowwise().mean();

	// step 2
	// Form matrix B hat
	B = B.colwise() - mu_B;
	// append additional column
	B.conservativeResize(NoChange, B.cols() + 1);
	B.col(B.cols()-1) = sqrt(n*m / (n+m)) * (mu_B - mu_A);

	// step 3
	// compute B tilde
	MatrixXd B_proj = U.transpose() * B;
	MatrixXd B_res = B - U * B_proj;
	HouseholderQR<MatrixXd> qr(B_res);
	MatrixXd thinQ(MatrixXd::Identity(B_res.rows(),B_res.cols()));
	MatrixXd B_tilde = qr.householderQ() * thinQ;

	// R
	MatrixXd R(sigma.size()+B_tilde.cols(), sigma.size()+B.cols());
	MatrixXd sigma_m = sigma.asDiagonal();
	R << ff * sigma_m, B_proj,
			MatrixXd::Zero(B_tilde.cols(),sigma.size()), B_tilde.transpose() * B_res;

	// step 4
	// SVD of R
	JacobiSVD < MatrixXd > svd(R, ComputeThinU | ComputeThinV);
	MatrixXd Q(U.rows(), U.cols() + B_tilde.cols());
	Q << U, B_tilde;
	U = svd.matrixU();
	sigma = svd.singularValues();


	// step 5
	// Remove small singular values
	double cutoff = sigma.norm() * 1e-3; // same as ivt implementation
	int keep; // the number of columns to keep in U
	double val;
	// Note that sigma is in descending order
	int i;
	for (i = 0 ; i < sigma.size(); i++){
		val = *(sigma.data() + i);
		if (val <= cutoff) break;
	}

	// check U and sigma
	keep = i > max_n_eig ? max_n_eig : i ;// at most max_n_eig singular values
	sigma.conservativeResize(keep);
	U = Q * U.leftCols(keep);

	// update the mean image
	mu_A = (ff * n * mu_A + m * mu_B) / (ff*n + m);

	// update n <- fn + m
	n = ff *n + m;

}

/*! entry condition: addi_patches is updated
   It updates:
			- mean_prev_patches
			- n_prev_patches
			- U
			- sigma
*/
void PCA::incrementalPCA() {
  sklm(U, sigma, mean_prev_patches, addi_patches, n_prev_patches, f_factor, n_eigenvec);
}

void PCA::display_basis() {
	// The size of U is (resX * resY) x cols(16 at most).
	// to display, first reshape the matrix U to (resY * 4) by (resX * 4)
	// pad zeros if no data
	int m = getResY();
	int n = sqrt(n_eigenvec);
	int row = m *  n;
	int col = m *  n;
	double min_val; // for computing the normalization parameters
	double max_val;
	double range;
	cv::Mat dst(row, col, CV_64FC1, cv::Scalar(0));
	MatrixXd U_tmp = U;
	MatrixXd mean_tmp = mean_prev_patches;

	cv::Mat mean_img(m, m, CV_64FC1, mean_tmp.data());
	cv::Mat mean_uchar(m, m, CV_8UC1);
	cv::Mat mean_uchar_bigger(5*m, 5*m, CV_8UC1);
	min_val = mean_tmp.colwise().minCoeff().minCoeff();
	max_val = mean_tmp.colwise().maxCoeff().maxCoeff();
	range = max_val - min_val;
	mean_img.convertTo(mean_uchar, mean_uchar.type(), 255./range, -255.*min_val/range);
	cv::resize(mean_uchar, mean_uchar_bigger, mean_uchar_bigger.size() );
	imshow("mean image", mean_uchar_bigger);
	// Reshape the basis images into a 'row' by 'col' grid
	for (int i = 0; i< U_tmp.cols(); i++){
		cv::Mat sub_img(m, m, CV_64FC1, U_tmp.col(i).data() );
		sub_img.copyTo(dst(cv::Rect( (i%n)*m, (i/n)*m,  m, m)));
	}

	minMaxLoc(dst, &min_val, &max_val);
	range = max_val - min_val;
	// Construct Mat object for the uint8 gray scale patch
	cv::Mat dst_uchar(row, col, CV_8UC1);
	// Convert the RGB patch to grayscale
	dst.convertTo(dst_uchar, dst_uchar.type(), 255./range, -255.*min_val/range);
	// set the empty basis back to zeros after the conversion
	if ( U_tmp.cols() < n_eigenvec ) {
		for (int i = U_tmp.cols(); i< n_eigenvec;i++){
			dst_uchar(cv::Rect( (i%n)*m, (i/n)*m,  m, m)).setTo(0);
		}
	}
	cv::Mat dst_bigger(5*row, 5*col, CV_8UC1);
	cv::resize(dst_uchar, dst_bigger, dst_bigger.size() );
	imshow("Basis Images", dst_bigger);
}

/*!
 *  Display the intermediate results
 *  1, current image
 *  1, template
 *  2, reconstructed image
 *
 *  	@param curr_image mean-subtracted image
 *		@param error_image difference between the current image and the reconstructed
 *
 */
void PCA::display_images(VectorXd &curr_image, VectorXdM &error_image) {
	double min_val;
	double max_val;
	double range;

	// Visualize the current image
	cv::Scalar gt_color(0, 255, 0);
	VectorXd curr_image_full = curr_image + mean_prev_patches;
	cv::Mat curr_cv = cv::Mat(getResY(), getResX(), CV_64FC1, curr_image_full.data());
	// Construct Mat object for the uint8 gray scale patch
	cv::Mat curr_unchar(getResY(), getResX(), CV_8UC1);
	min_val = curr_image_full.colwise().minCoeff().minCoeff();
	max_val = curr_image_full.colwise().maxCoeff().maxCoeff();
	range = max_val - min_val;
	// Convert the RGB patch to grayscale
	curr_cv.convertTo(curr_unchar, curr_unchar.type(), 255./range, -255.*min_val/range); // normalize the data to [0,255]
	//minMaxLoc(curr_unchar, &min_val, &max_val);
	//range = max_val - min_val;
	cv::Mat curr_unchar_bigger(5*getResY(), 5*getResX(), CV_8UC1); // why 5?
	cv::resize(curr_unchar, curr_unchar_bigger, curr_unchar_bigger.size() );
	cv::putText(curr_unchar_bigger, "Current Image", cv::Point2d(5,15),
					cv::FONT_HERSHEY_SIMPLEX, 0.5 , gt_color);
	imshow("Current Image", curr_unchar_bigger);

	// Visualize the reconstructed image
	VectorXd recon_image = U*(U.transpose()*curr_image) + mean_prev_patches;
	cv::Mat curr_recon_cv = cv::Mat(getResY(), getResX(), CV_64FC1, recon_image.data());
	// Construct Mat object for the uint8 gray scale patch
	cv::Mat curr_recon_cv_uchar(getResY(), getResX(), CV_8UC1);
	cv::Mat curr_recon_cv_uchar_bigger(5*getResY(), 5*getResX(), CV_8UC1);
	// Convert the RGB patch to grayscale
	min_val = curr_image_full.colwise().minCoeff().minCoeff();
	max_val = curr_image_full.colwise().maxCoeff().maxCoeff();
	range = max_val - min_val;
	curr_recon_cv.convertTo(curr_recon_cv_uchar, curr_recon_cv_uchar.type(),255./range, -255.*min_val/range);
	cv::resize(curr_recon_cv_uchar, curr_recon_cv_uchar_bigger, curr_recon_cv_uchar_bigger.size() );
	cv::putText(curr_recon_cv_uchar_bigger, "Reconstructed Image", cv::Point2d(5,15),
					cv::FONT_HERSHEY_SIMPLEX, 0.5 , gt_color);
	imshow("Reconstructed Image", curr_recon_cv_uchar_bigger);

	// Visualize the error image
	VectorXd err_img = error_image;
	cv::Mat err_cv = cv::Mat(getResY(), getResX(), CV_64FC1, err_img.data());
	// Construct Mat object for the uint8 gray scale patch
	cv::Mat err_cv_uchar(getResY(), getResX(), CV_8UC1);
	cv::Mat err_cv_uchar_bigger(5*getResY(), 5*getResX(), CV_8UC1);
	// Convert the RGB patch to grayscale
	// normalize to [0,255]
	min_val = err_img.colwise().minCoeff().minCoeff();
	max_val = err_img.colwise().maxCoeff().maxCoeff();
	range = max_val - min_val;
	err_cv.convertTo(err_cv_uchar, err_cv_uchar.type(), 255./range, -255.*min_val/range);
	cv::resize(err_cv_uchar, err_cv_uchar_bigger, err_cv_uchar_bigger.size() );
	cv::putText(err_cv_uchar_bigger, "Error Image", cv::Point2d(5,15),
					cv::FONT_HERSHEY_SIMPLEX, 0.5 , gt_color);
	imshow("Error Image", err_cv_uchar_bigger);
}

_MTF_END_NAMESPACE

